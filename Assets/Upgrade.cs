﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUpgrade
{
	long Process(long score, Player player);
}


public class Upgrade1: IUpgrade
{
	private int _multiplicator;

	public Upgrade1(int multiplicator)
	{
		_multiplicator = multiplicator;
	}

	public long Process(long score, Player player)
	{
		return score * (long) _multiplicator;
	}
	
}

public class Player
{
	private List<IUpgrade> _upgrades = new List<IUpgrade>();

	public Player()
	{
	}

	public Player(List<IUpgrade> upgrades)
	{
		_upgrades = upgrades;
	}

	public void UpgradeAdd(IUpgrade upgrade)
	{
		_upgrades.Add(upgrade);
	}

	public void UpgradeRemove(IUpgrade upgrade)
	{
		_upgrades.Remove(upgrade);
	}

	public List<IUpgrade> Upgrades
	{
		get { return _upgrades; }
		set { _upgrades = value; }
	}
}