﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public int DiskChangeTime = 5;

    public GameObject TimerText;
    public GameObject ScoreText;
    public GameObject LastTimerText;
    public GameObject InfoText;

    private Player _player;
    private int _diskNumber;
    private long _score;
    private DateTime _lastTime;

    // Use this for initialization
    void Start()
    {
        _diskNumber = 1;
        _lastTime = DateTime.Now;
        _score = 0;
        _player = new Player();
    }

    // Update is called once per frame
    void Update()
    {
        SetText(ScoreText, FormatText(_score.ToString()));
        SetText(LastTimerText, FormatText((DateTime.Now - _lastTime).TotalSeconds.ToString()));
        SetText(InfoText, "Please Insert Floppy #" + _diskNumber);
    }

    public void NextFloppy()
    {
        if (CheckFloppy())
        {
            _diskNumber++;
            RewardPlayer();
        }
    }

    private void RewardPlayer()
    {
        long score = 1;
        foreach (IUpgrade upgrade in _player.Upgrades)
        {
            score = upgrade.Process(score, _player);

        }
        _score+= score;
    }

    private bool CheckFloppy()
    {
        if (DateTime.Now > _lastTime.AddSeconds(DiskChangeTime))
        {
            _lastTime = DateTime.Now;
            return true;
        }

        return false;
    }

    private void SetText(GameObject textGameObject, string value)
    {
        textGameObject.GetComponent<Text>().text = value;
    }

    private string FormatText(string text)
    {
        var texts = text.Split('.');
        return texts[0];
    }

    public void BuyUpgrade1()
    {
        _player.UpgradeAdd(new Upgrade1(2));
    }


}